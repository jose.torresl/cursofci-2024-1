#Librerias fundamentales
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class RKG:
    """
    Clase RKG: Permite solucionar de forma numérica una ecuación diferencial ordinaria de primer orden
    por un método de Runge-Kutta generalizado.
    """
#El constructor de la clase recibe como entrada la función que define la derivada, las condiciones
#iniciales asociadas al problema, el valor final hasta donde se van a calcular las soluciones y el tamaño del paso.

#x0: Valor inicial en la coordenada en x (float).
#y0: Valor inicial en la coordenada en y (float)

    def __init__(self, Fun, x0, y0, x_final, h, a, b, c):
        self.Fun = Fun   #Fun: Funcion objetivo expresada de la forma dy/dx = f(x,y). 
        self.x0 = x0   #x0: Valor inicial en la coordenada en x (float).
        self.y0 = y0   #y0: Valor inicial en la coordenada en y (float)
        self.x_final = x_final      #x_final: Valor final en la coordenada en x (float)
        self.h = h      # Tamaño del paso relacionado a la precision del metodo
        #a,b,c: Coeficientes asociados a las tablas de Butcher
        self.a = a
        self.b = b
        self.c = c

#La funcion "ks" calcula los distintos factores de K necesarios para llegar a 
#la solución por Runge-Kutta generalizado. El calculo se realiza paso a paso
    
    def ks(self, xi, yi, h, **kwargs):

        k = [0] * len(self.b)
        for i in range(len(self.b)):
            sumatoria = sum(self.a[i][j] * k[j] for j in range(i))
            k[i] = self.Fun(xi + self.c[i] * h, yi + h * sumatoria, **kwargs)
        return k

#La funcion "soluciones" calcula la solucion correspondiente a cada xi hasta un valor final
    def soluciones(self):
        xi = self.x0
        yi = self.y0
        X = []
        Y = []
        while xi < self.x_final:
            X.append(xi)
            Y.append(np.array(yi))
            k = self.ks(xi, yi, self.h)
            suma_pesos = sum(self.b[i] * k[i] for i in range(len(self.b)))
            yi += self.h * suma_pesos
            xi += self.h
        return X, np.array(Y)
    
#HERENCIA POO
#Clase creada utilizando el concepto de herencia con el objetivo de resolver simultaneamente
#3 ecuaciones diferenciales ordinarias acopladas
class RKGA(RKG):
    def __init__(self, Fun, x0, y0, x_final, h, a, b, c, sigma, rho, beta):
        super().__init__(Fun, x0, y0, x_final, h, a, b, c)
        self.sigma = sigma
        self.rho = rho
        self.beta = beta

    def soluciones(self):
        xi = self.x0
        yi = np.array(self.y0, dtype=float)
        X = []
        Y = []

        while xi <= self.x_final:
            X.append(xi)
            Y.append(yi.copy())

            k = [np.zeros_like(yi) for _ in range(len(self.b))]  

            for i in range(len(self.b)):
                sumatoria = np.zeros_like(yi) 
                for j in range(i):
                    sumatoria += self.a[i][j] * k[j]
                k[i] = self.Fun(xi + self.c[i] * self.h, yi + self.h * sumatoria, self.sigma, self.rho, self.beta)

            suma_pesos = np.dot(self.b, k)  
            yi += self.h * suma_pesos
            xi += self.h

        return X, np.array(Y)
    
    
    











